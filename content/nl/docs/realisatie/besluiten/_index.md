---
title: Besluiten
# menu: {main: {weight: 40}}
type: besluiten
weight: 80
---

Dit is de **besluiten** sectie, oftewel de 'DRs', Decision Records. Decision Records zijn een
methode om gestructureerd besluiten te maken op onderwerpen en deze vast te leggen. Hieronder vind
je het overzicht van alle besluiten, Decision Records, in chronologische volgorde.

Meer toelichting over Decision Records vind je in het besluit om deze methode toe te passen: [00002
Gebruik van Markdown Decision Records in Git](00002-decision-records/)
