---
title: Interoperabiliteit datamodellen en -formaten
weight: 2
date: 2023-10-04
tags: 
description: Het vermogen van FDS om data uitwisselbaar te maken op basis van afgesproken formaten en modellen
---

{{< buildingblock
    label="Interoperabiliteit"
    color="secondary"
    desc="Interoperabiliteit." >}}
  {{< buildingblock 
        label="API's voor data uitwisseling" 
        link="../data-exchange-api" />}}
  {{< buildingblock
        label="Data- en informatiemodellen en -formaten"
        color="info" />}}
  {{< buildingblock
        label="Herleidbaarheid en traceerbaarheid"
        link="../provenance-tracebility" />}}
{{< /buildingblock >}}

**Interoperabiliteit | Data Modellen en Formaten**: 
Deze bouwsteen stelt gemeenschappelijk formaten voor gegevens vast,
modelspecificaties en de representatie van gegevens in bij gegevensuitwisseling. Gecombineerd met
de bouwsteen Data Exchange API's zorgt dit bouwblok voor volledige interoperabiliteit tussen deelnemers.


## Architecture building blocks
Afspraken over formaten spelen in FDS, als afspraken stelsel, een
belangrijke rol.  Ze vormen deels het fundament onder de strategie van
FDS: het laten ontstaan van een afsprakenstelsel.  Een belangrijke
aspraak is dat Databronnen via een selfdescription aan het stelsel
angeboden worden. Het formaat van die selfdescription ligt vast in het
[self-description formaat van een databron](/docs/architectuur/capabilities/interoperabiliteit/selfdescription-aanmelding/sd-dbron/). Daarnaast
kan ook ook de datakwaliteti van de bron vastgelegd worden, en ook dat vindt plaats via een [self-description](/docs/architectuur/capabilities/interoperabiliteit/selfdescription-aanmelding/dq-dbron/).
Het vastleggen van deze informatie vindt plaats door de FDS-participant. Er vindt een beoordeling plaats van de aangeboden bron door de FDS-poortwachter.
Die beoordeling vindt deels geautomatiseerd plaats, via de Beoordelingsvoorziening, en is deels een beoordeling door de poortwachter zelf. De beschrijvingen vand e databronnen en de beschrijvingen van de bijbehorende datakwaliteit worden opgeslagen in een opslagvoorziening.

![ABB databron formaten](images/abbssfmt.png) 
 
 
### Revisieafspraken

>>> Dit gaat naar een ander hoofdstuk

Voor alle formaten die binnen FDS voorgeschreven worden geldt dat er
een actueel formaat is, en dat de voorgaande versie van het
voorgeschreven formaat geldt gedurende de periode (de grace period)
die aangegeven wordt hierboven.  Deelname aan FDS betekent dat de
participanten zich verplichten in de grace period hun aanbod en
systemen om te stellen naar de nieuwe versie. Voor deze omstelling
kunnen geen kosten in rekening gebracht worden. Het FDS team is,
wanneer noodzakelijk, beschikbaar voor inhoudelijk advies ten aanzien
van de omstelling.
