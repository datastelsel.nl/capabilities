---
title: Data bij de bron
weight: 2
description: Gebruik van transparante en actuele gegevens
---
## Gegevens direct uit de bron
Het [data bij de
bron](https://www.digitaleoverheid.nl/data-bij-de-bron/) principe
vormt een basis concept voor de digitalisatie van de Rijksoverheid, en
ook van FDS. We maken gebruik van data zoals deze actueel in de
systemen van de bronhouder beschikbaar zijn, en werken niet met aparte
kopieën. Dat zorgt voor een betrouwbare gegevensvoorziening.

