---
title: Dataservices
weight: 2
date: 2023-10-17
categories: [Capabilities]
tags: 
description: >
  Interoperabiliteit | Dataservices: API's voor data uitwisseling
---

{{< capabilities-diagram selected="dataservices" >}}

**Interoperabiliteit | Dataservices**:

API's voor data uitwisseling ...

Deze bouwsteen vergemakkelijkt het delen en uitwisselen van gegevens door participanten van de dataruimte. 

## Bouwblokken

- [Dataservice](/docs/realisatie/bouwblokken/dataservice/)
- [Self-Description]

## Architecture building blocks
Twee participanten wisselen informatie uit in FDS op basis van
API's. Daarvoor stelt de aanbiedende participant een API beschikbaar
die door de vragende participant rechtstreeks benaderd wordt. Hiermee
wordt volledig voldaan aan het principe [data bij de bron](/docs/concepts/data-bij-de-bron/).  De
uitwisseling vindt plaats op basis van vooraf overeengekomen
certificaten welke bij elke transactie gecontroleerd worden (dit is
overeenkomstig de [NLx](https://nlx.io/) aanpak). Dit laatste is niet
in het onderstaande model opgenomen.

![API-view](/api-view.png)

<!--- 

Hier moeten we nog iets mee

Een voorbeeld van
een bouwsteen voor gegevensinteroperabiliteit die een gemeenschappelijke API voor gegevensuitwisseling biedt, is de ‘Context’
Broker’ van de Connecting Europe Facility (CEF)50, aanbevolen door de Europeaan
Commissie voor het delen van right-time data tussen meerdere organisaties.

--> 

<!-- Local Variables: -->
<!-- jinx-languages: "nl_NL" -->
<!-- End: -->
