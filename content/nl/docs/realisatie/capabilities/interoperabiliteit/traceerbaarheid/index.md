---
title: Traceerbaarheid
weight: 3
date: 2023-10-17
categories: [Capabilities]
tags: 
description: >
  Interoperabiliteit | Traceerbaarheid: Traceerbaarheid en herleidbaarheid
---

{{< capabilities-diagram selected="traceerbaarheid" >}}

**Interoperabiliteit | Traceerbaarheid**:

Traceerbaarheid en herleidbaarheid ...

Deze bouwsteen biedt de middelen voor tracering en
het volgen van het proces van dataverstrekking en dataverbruik/-gebruik. Het biedt daarmee de
basis voor een aantal belangrijke functies, van identificatie van de gegevensset tot audit-
bewijsregistratie van transacties. Het maakt ook de implementatie van een breed scala aan tracking mogelijk, zoals het volgen van producten of materiaalstromen in een leveringsketen.




<!-- Local Variables: -->
<!-- jinx-languages: "nl_NL" -->
<!-- End: -->
