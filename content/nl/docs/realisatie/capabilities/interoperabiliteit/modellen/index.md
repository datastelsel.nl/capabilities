---
title: Modellen
weight: 1
date: 2023-10-17
categories: [Capabilities]
tags: 
description: >
  Interoperabiliteit | Modellen: Data- en informatiemodellen en -formaten
---

{{< capabilities-diagram selected="modellen" >}}

**Interoperabiliteit | Modellen**:

Data- en informatiemodellen en -formaten ...
