---
title: Governance
weight: 1
date: 2023-09-13
categories: [Capabilities]
tags: 
description: >
  Governance capabilities
---

Hoofdcategorie capabilities **Governance**!

{{< capabilities-diagram selected="governance" >}}
