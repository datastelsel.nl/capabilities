---
title: Vertrouwen
weight: 3
date: 2022-10-25
categories: [Capabilities]
tags: 
description: Vertrouwen bouwblokken
---

Hoofdcategorie bouwblok **Vertrouwen**!

Bouwblokken die data-souvereiniteit en vertrouwen faciliteren met aspecten als identiteitsbeheer, betrouwbaarheid van deelnemers, evenals data-toegangs- en data-gebruikscontrole aan bod komen.

{{< capabilities-diagram selected="vertrouwen" >}}
