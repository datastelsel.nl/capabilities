---
title: Veiligheid
weight: 3
date: 2023-10-17
categories: [Capabilities]
tags: 
description: >
  Vertrouwen | Veiligheid: Vertrouwde uitwisseling
---

{{< capabilities-diagram selected="veiligheid" >}}

**Vertrouwen | Veiligheid**:

Een vertrouwde data-uitwisseling tussen deelnemers, waardoor deelnemers aan een
data-uitwisselingstransactie ervan verzekerd zijn dat andere deelnemers werkelijk zijn wie ze
beweren te zijn en dat ze zich houden aan gedefinieerde regels/afspraken. Dit kan worden bereikt
door organisatorische maatregelen (bijvoorbeeld certificering of geverifieerde inloggegevens) of
technische maatregelen (bijvoorbeeld attestatie op afstand).
