---
title: Welkom
weight: 10
# menu: {main: {weight: 20}}
description: Wat kan ik hier vinden? 
categories: 
---

Welkom bij federatief.datastelsel.nl, het platform voor ons allemaal!

Het federatief datastelsel is een open source project dat voor iedereen toegankelijk is om te
gebruiken en te verbeteren. De ontwikkeling hiervan doen we juist graag met elkaar, dus we kijken
uit naar je betrokkenheid.

Hieronder vind je twee documenten die inzicht geven in hoe we hier met elkaar willen samenwerken. In
[strategie van samenwerken](/docs/welkom/strategie-van-samenwerken/) en de
[werkomgeving](/docs/welkom/werkomgeving/) hopen we jullie inzicht en duidelijkheid hierin te geven. 

Om te beginnen zijn er grofweg drie hoofdcategorieën voor aanvullende informatie:

- **[Docs](/docs/)**
  
  Hier staan alle documenten en documentatie die 'vaste onderdelen' van het federatief datastelsel
  beschrijven. Ook onze [Contribution Guidelines](/docs/community/contribution/) en [Docs /
  Community / Hulp / Markdown](/docs/community/hulp/markdown/) zijn hier te vinden.

  Een belangrijk hoofdstuk in **Docs** zijn de **[capabilities](/docs/realisatie/capabilities/)**
  van het federatief datastelsel.

- **[Besluiten](/besluiten/)**
  
  Besluiten en afwegingen die tot besluiten leiden of hebben geleid, vastgelegd in [decision
  records](/docs/realisatie/besluiten/00002-decision-records/), zijn hier terug te vinden.

- **[Blogs](/blog/)**
  
  Ideeën, suggesties, eerste schetsen, 2-pagers, tijdelijke documenten ... en meer van dit is hier
  te vinden. Check bijvoorbeeld ook ons [Raamwerk voor ontwikkeling in
  samenwerking](/docs/realisatie/raamwerk/)

Daarnaast zijn er ook een aantal aanvullende kanalen waar je terecht kunt:

- **[MatterMost](https://digilab.overheid.nl/chat)**
  
  Dit is het chatomgeving waar alle vragen over het federatief datastelsel gevraagd kunnen worden.
  MatterMost is daarnaast ook de plek waar andere projecten samenwerken, waaronder Digilab. Als je
  ook met Digilab wilt chatten, kan dat in dezelfde omgeving via deze link.

- **[Realisatie IBDS Pleio omgeving](/realisatieIBDSpleioomgeving/)**
  
  Op deze pagina vind je alle informatie over het programma Realisatie IBDS, dit programma bestaat
  uit vier pijlers die allemaal hier hun informatie delen. Zo heeft ook het federatief datastelsel,
  een onderdeel in dit programma, haar eigen pleio omgeving waar je vragen kunt vinden, maar ook
  vergaderstukken, notulen en documenten. 

