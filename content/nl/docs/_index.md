---
title: Documentatie
# linkTitle: Docs
menu: {main: {weight: 30}}
weight: 20
---

Op deze pagina vind je alle documenten die tot nu toe ontwikkeld zijn. 

> Mogelijk andere info die je interessant vindt:
> 
> - [Blog/Concepten](/blog/concepten/) voor concepten nog in ontwikkeling. Lever hier dus vooral je
>   input op via Git of stel vragen via Mattermost (zie [Werkomgeving](/docs/welkom/werkomgeving/))
> - [Besluiten](/docs/realisatie/besluiten) voor onze overwegingen en afwegingen die tot besluiten hebben geleid.
>   Hier kun je in Mattermost vragen over stellen, maar dient ter informatie waarom bepaalde keuzes
>   gemaakt zijn. 

Check vooral onze [Community / Hulp](/docs/community/hulp) voor meer info over hoe je kunt bijdragen.
