---
title: Community
description: De community dat zijn we samen!
categories: 
tags: 
---

Een federatief datastelsel ontwikkel je niet met een enkele organisatie, maar met elkaar. Het woord 'community' is hot en wordt vaak genoemd, maar wat bedoelen wij er nu precies mee?

Een community is het actief betrekken en informeren van diverse doelgroepen zowel offline als online, waarbij je ook een actieve bijdrage verwacht van iedereen die participeert op dit platform. Het gaat dus om een gezonde balans tussen brengen en halen en het creeeren van draagvlak voor wat het federatief datastelsel betekent. Dit moet resulteren in een wederzijdse relatie tussen ons en onze omgeving. 

Voel jij je aangesproken en wil je meteen al actief bijdragen? Hieronder zie je bij contributie (nog link toevoegen) hoe je dit kunt doen. 
 
**Informeren**
Niet iedereen zal meteen actief willen bijdragen, maar eerst meer informatie willen lezen over het federatief datastelsel. Dat is helemaal goed. Klik erop los bij alle documenten aan de linkerkant. Voor nu alleen even behoefte aan meer informatie? Naast dit platform kun je ook hier nog informatie vinden: 
1.	Realisatie IBDS Pleio (LINK): hier vind je generieke informatie over het programma IBDS, waar de pijler federatief datastelsel ondervalt. In deze omgeving deelt het programma evenementen en relevante nieuws artikelen. 
2.	LinkedIn (LINK): hier vind je het formele LinkedIn kanaal van het programma, volg ons hier om op de hoogte te blijven van ons meest belangrijke nieuws en updates. 

