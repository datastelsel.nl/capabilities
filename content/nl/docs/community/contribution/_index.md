---
title: Contributie
weight: 10
description: Hoe bij te dragen tot de documentatie
---

<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/welkom/">
  Welkom <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-primary me-3 mb-4" href="/docs/welkom/werkomgeving/">
  Werkomgeving <i class="fas fa-arrow-alt-circle-right ms-2"></i>
</a>
<a class="btn btn-lg btn-secondary me-3 mb-4" href="https://gitlab.com/datastelsel.nl/ontwikkeling/ontwikkeling-website" target="_blank">
  Sources <i class="fab fa-gitlab ms-2 "></i>
</a>

Bovenstaande links zijn goede startpunten, afhankelijk van wat je zoekt en/of nodig hebt.
