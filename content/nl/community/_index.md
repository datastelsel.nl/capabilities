---
title: Community
menu: {main: {weight: 40}}
---

{{% blocks/lead color="primary" %}}
federatief.datastelsel.nl is een open source project welke iedereen kan gebruiken, verbeteren en van
kan genieten. We zouden het fijn vinden als je betrokken wordt! Hier zijn een aantal manieren om
erachter te komen wat er speelt en hoe je betrokken kunt worden.
{{% /blocks/lead %}}