---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
categories: [DR]
tags: 
status: draft
description: "{{ description }}"
---

## Context en probleemstelling

[...]

## Beslissingsfactoren <!-- optional -->

- … <!-- numbers of drivers can vary -->

## Overwogen opties

- Optie 1
- Optie 2
- … <!-- numbers of options can vary -->

## Besluit

In datastelsel.nl [...].

### Positieve gevolgen <!-- optional -->

- … <!-- numbers of options can vary -->

### Negatieve Consequences <!-- optional -->

- … <!-- numbers of options can vary -->

## Pros en Cons van de oplossingen <!-- optional -->

### Optie 1

> // TODO ...

## Links <!-- optional -->

- [Link naam](link to adr) <!-- example: Refined by [xxx](yyyymmdd-xxx.md) -->
- … <!-- numbers of links can vary -->
