FROM docker.io/klakegg/hugo:ext-alpine
# FROM registry.gitlab.com/pages/hugo/hugo_extended:0.119.0

RUN apk add git && \
  git config --global --add safe.directory /src
